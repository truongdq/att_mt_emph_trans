#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 truong-d <truong-d@ahclab40>
#
# Distributed under terms of the MIT license.

"""

"""
from __future__ import print_function
import chainer
import component as com
import chainer.functions as F
import chainer.links as L
from chainer import cuda
import numpy as np
import os
from chainer import serializers
import json
import ConfigParser
from chainer import optimizers

xp = np

class Trainer(object):
    def __init__(self, grad_clip=5., opt_type="Adam", lr=0.001, lr_descend=1.8, lr_thres=0.00001):
        print("~~~~~ Training paramerters ~~~~~~~")
        print('~ Inital lr: %5f' % (lr))
        print('~ lr descend rate: %f' % (lr_descend))
        print('~ lr stop: %5f' % (lr_thres))
        if opt_type == 'Adam':
            print('~ Optimizer: Adam')
            self.optimizer = optimizers.Adam()
        elif opt_type == 'RMSpropGraves':
            print('~ Optimizer RMSpropGraves')
            self.optimizer = optimizers.RMSpropGraves(lr=lr)
        else:
            print('Optimizer %s not supported' % opt_type)
            exit(1)
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

        self.grad_clip = grad_clip
        self.lr_thres = lr_thres
        self.accum_loss = 0
        self.lr_descend = lr_descend
        self.model = None 
    
    def continue_train(self):
        if self.optimizer.lr < self.lr_thres:
            return False
        else:
            return True

    @property
    def lr(self):
        return self.optimizer.lr
    
    @lr.setter
    def lr(self, value):
        self.optimizer.lr = value

    def descend_lr(self):
        print('Descending')
        self.optimizer.lr /= self.lr_descend

    def setup(self, model):
        self.model = model
        self.optimizer.setup(self.model)
        self.optimizer.add_hook(chainer.optimizer.GradientClipping(self.grad_clip))

    def update_params(self):
        if not isinstance(self.accum_loss, int):
            self.model.zerograds()
            self.accum_loss.backward()
            self.accum_loss.unchain_backward()  # truncate
            self.accum_loss = 0
            self.optimizer.update()

    def add_loss(self, loss):
        self.accum_loss += loss

class DoNet(chainer.Chain):
    def __init__(self, isize, hsize, osize, depth=1, \
            drop_ratio=0., src_vocab=None, trg_vocab=None, att_type="dot"):
        print('~~~~~~~ Model parameters ~~~')
        print('~ Input size:', isize)
        print('~ Output size:', osize)
        print('~ Hidden size:', hsize)
        print('~ Attention  :', att_type)
        print('~ Layer depth:', depth)
        print('~ Drop ratio:', drop_ratio)
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~')

        self.isize = isize
        self.hsize = hsize
        self.osize = osize
        self.depth = depth
        self.drop_ratio = drop_ratio
        self.src_vocab = src_vocab
        self.trg_vocab = trg_vocab
        enc = com.MTEncoder(isize, hsize, depth, drop_ratio)
        dec = com.MTDecoder(hsize, osize, depth, drop_ratio)
        att = com.MTAttention(hsize, att_type=att_type)
        e2h = L.Linear(isize, hsize)
        src_emb = L.EmbedID(len(src_vocab), isize)
        trg_emb = L.EmbedID(len(trg_vocab), isize)
        super(DoNet, self).__init__(enc=enc, att=att, dec=dec, e2h=e2h, src_emb=src_emb, trg_emb=trg_emb)
        self.S = None
        self._train = True
    
    @property
    def train(self):
        return self._train
    
    @train.setter
    def train(self, value):
        self._train = value
        self.enc.train = value
        self.dec.train = value

    def to_gpu(self):
        global xp
        xp = cuda.cupy
        super(DoNet, self).to_gpu()

    def encode(self, x_batch):
        assert self.src_emb is not None
        x_list = []
        for i in xrange(x_batch.shape[1]):
            src_w = chainer.Variable(xp.asarray(x_batch[: , i], dtype=np.int32), volatile=not self.train)
            src_w = self.src_emb(src_w)
            x_list.append(src_w)

        self.S, e_l = self.enc(x_list)
        self.dec.reset(e_l)

    def reset(self):
        self.S = None
        self.enc.reset()
        self.dec.reset()
        self.att.reset()

    def decode(self, x, no_att=False):
        h = self.dec.update(self.e2h(x))
        if no_att:
            o = self.dec()
        else:
            att = self.att(self.S, h)
            o = self.dec(att_in=att)
        return o
    
    @classmethod
    def load(cls, model_dir):
        # Input files
        print('Loading model from:', model_dir)
        fn_model = os.path.join(model_dir, 'donet.mdl')
        fn_cfg = os.path.join(model_dir, 'donet.cfg')
        fn_src_vocab = os.path.join(model_dir, 'src_vocab.txt')
        fn_trg_vocab = os.path.join(model_dir, 'trg_vocab.txt')
        fn_model = os.path.join(model_dir, 'donet.mdl')
        fn_cfg = os.path.join(model_dir, 'donet.cfg')

        config = ConfigParser.RawConfigParser()
        config.read(fn_cfg)
        isize = config.getint('Structure', 'isize')
        hsize = config.getint('Structure', 'hsize')
        osize = config.getint('Structure', 'osize')
        depth = config.getint('Structure', 'depth')
        drop_ratio = config.getfloat('Structure', 'drop_ratio')

        src_vocab = json.load(open(fn_src_vocab))
        trg_vocab = json.load(open(fn_trg_vocab))

        nnet = cls(isize, hsize, osize, depth, drop_ratio, src_vocab, trg_vocab)
        serializers.load_npz(fn_model, nnet)

        return nnet

    def save(self, model_dir):
        if not os.path.exists(model_dir):
            os.makedirs(model_dir)

        config = ConfigParser.RawConfigParser()
        config.add_section('Structure')
        config.set('Structure', 'isize', str(self.isize))
        config.set('Structure', 'hsize', str(self.hsize))
        config.set('Structure', 'osize', str(self.osize))
        config.set('Structure', 'depth', str(self.depth))
        config.set('Structure', 'drop_ratio', str(self.drop_ratio))

        # Output files
        fn_model = os.path.join(model_dir, 'donet.mdl')
        fn_cfg = os.path.join(model_dir, 'donet.cfg')
        fn_src_vocab = os.path.join(model_dir, 'src_vocab.txt')
        fn_trg_vocab = os.path.join(model_dir, 'trg_vocab.txt')

        # Write out dictionaries
        if self.src_vocab:
            with open(fn_src_vocab, 'w') as fd_src_vocab:
                json.dump(self.src_vocab, fd_src_vocab)

        if self.trg_vocab:
            with open(fn_trg_vocab, 'w') as fd_trg_vocab:
                json.dump(self.trg_vocab, fd_trg_vocab)
        
        # Write out models
        print('Saving model to:', fn_model)
        serializers.save_npz(fn_model, self)
        
        with open(fn_cfg, 'w') as fd_cfg:
            config.write(fd_cfg)
