#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 truong-d <truong-d@ahclab08>
#
# Distributed under terms of the MIT license.

"""

"""
from __future__ import print_function
import codecs
from collections import OrderedDict, defaultdict
from collections import Counter
from chainer import Variable
import six
import numpy as np
import traceback
import sys


def normalize_data(data, vocab):
    for sen in data:
        for i in range(len(sen)):
            w = sen[i]
            if w not in vocab:
                sen[i] = vocab["<unk>"]
            else:
                sen[i] = vocab[w]

def count_word(data):
    word_count = defaultdict(lambda: 0)
    for sen in data:
        for w in sen:
            word_count[w] += 1
    return word_count


def load_dev_data(fname, src_vocab, trg_vocab):
    src_raw, trg_raw = load_data(fname) 
    normalize_data(src_raw, src_vocab)
    normalize_data(trg_raw, trg_vocab)
    return {'input': src_raw, 'target': trg_raw}

def load_train_data(fname, src_vocab, trg_vocab, unk_thres=1, w_count=defaultdict(lambda: 0)):
    src_raw, trg_raw = load_data(fname, w_count=w_count)
    refine_data(src_raw, src_vocab, unk_thres, w_count)
    refine_data(trg_raw, trg_vocab, unk_thres, w_count)
    return {'input': src_raw, 'target': trg_raw}

def load_data(fname_input, w_count=defaultdict(lambda: 0)):
    all_data_src = []
    all_data_trg = []

    with open(fname_input, 'r') as fin:
        for line in fin:
            line = line.strip()
            src, trg = line.split('|||')

            src_data = []
            for w in src.split():
                w_count[w] += 1
                src_data.append(w)
            src_data.append("</s>")

            trg_data = []
            for w in trg.split():
                w_count[w] += 1
                trg_data.append(w)
            trg_data.append("</s>")

            all_data_src.append(src_data)
            all_data_trg.append(trg_data)
    return all_data_src, all_data_trg

def refine_data(data, vocab, unk_thres=1, w_count=defaultdict(lambda: 0)):
    for sen in data:
        for i in range(len(sen)):
            w = sen[i]
            if w != "</s>" and w_count[w] <= unk_thres:
                sen[i] = vocab["<unk>"]
            else:
                if w not in vocab:
                    vocab[w] = len(vocab)
                sen[i] = vocab[w]

def equalize_width(array, vocab):
    max_width = len(array[0])
    for row in array[1:]:
        max_width = max(max_width, len(row))
    mask = []
    for idx, row in enumerate(array):
        mask.append([1. for x in range(len(row))])
        for i in xrange(max_width - len(row)):
            row.append(vocab["<stuff>"])
            mask[-1].append(0.)
    return mask


def data_multi_row_spliter(dataset, batchsize, sort_utt=True):
    import copy

    def chunks(l, n):
        """Yield successive n-sized chunks from l."""
        for i in range(0, len(l), n):
            yield l[i:i + n]

    if sort_utt:
        sortedRes = sorted(zip(dataset['input'], dataset['target']),
                           key=lambda x: len(x[0]), reverse=True)
        x_data = [x[0] for x in sortedRes]
        y_data = [x[1] for x in sortedRes]
    else:
        x_data = dataset['input']
        y_data = dataset['target']

    for n_row_of_x, n_row_of_y in \
        zip(chunks(x_data, batchsize), chunks(y_data, batchsize)):
      yield n_row_of_x, n_row_of_y

def data_spliter(dataset, batchsize=1, sort_utt=True):
    return data_multi_row_spliter(dataset, batchsize, sort_utt=sort_utt)
