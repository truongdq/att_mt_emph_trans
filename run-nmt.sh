#! /bin/bash
#
# run.sh
# Copyright (C) 2016 truong-d <truong-d@ahclab40>
#
# Distributed under terms of the MIT license.
#

if [[ -z $1 ]]; then
    gpu=0
else
    gpu=$1
fi

echo "Using gpu $gpu"
# Model structure
bsize=64
epoch=30
emb_size=200
hid_size=512
att_type="dot"  # dot,concat,general
depth=3
drop_out=0.5

# Training parameter 
bprob_len=10       # Truncation length
lr=0.001           # Inital learning rate
lr_descend=1.8     # Descend the LR by this factor when loss on dev set start increasing
lr_stop=0.0001    # Stop training when the learning rate falls below this threshold

lang_pair=vi-en
#out_dir=exp/donet_nmt_${lang_pair}_e${emb_size}_h${hid_size}_d${depth}_b${bsize}_t${bprob_len}
out_dir=exp/donet_nmt_test
train=data/mt/${lang_pair}/train.10k
dev=data/mt/${lang_pair}/dev
test=data/mt/${lang_pair}/test

set -x
mkdir -p $out_dir

python train-nmt.py --train $train --dev $dev --test $test \
        --batchsize $bsize --epoch $epoch --emb-size $emb_size --hidden-size $hid_size \
        --depth $depth --drop $drop_out \
        --bprob-len $bprob_len --gpu $gpu \
        --lr $lr --lr-descend $lr_descend --lr-stop $lr_stop \
        --att-type $att_type \
        --out-model $out_dir  | tee $out_dir/training.log
