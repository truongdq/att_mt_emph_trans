#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 truong-d <truong-d@ahclab40>
#
# Distributed under terms of the MIT license.

"""

"""
from __future__ import print_function
import numpy as np
import chainer
from donet import donet
from donet.lib_data import load_train_data, load_dev_data, data_spliter, equalize_width
import optparse
import sys
np.random.seed(0)

parser = optparse.OptionParser('%s model_dir [-|file]' % (sys.argv[0]))
parser.add_option("--realtime", action="store_true", default=False)
parser.add_option("--batchsize", dest="bsize", default=1,
        type="int")
parser.add_option('--genlimit', help="Maximum translated text length", type='int', default=100)
opts, args = parser.parse_args()

if not args:
    parser.print_help()
    exit(1)
model_dir = args[0]
model = donet.DoNet.load(model_dir)
model.train = False
trg_vocab_reverse = {}
for w, key in model.trg_vocab.items():
    trg_vocab_reverse[key] = w

fd_in = sys.stdin
if len(args) > 1:
    fd_in = open(args[1])


def text2num(text):
    text = text.decode('utf-8')
    output = []
    for w in text.split():
        if w in model.src_vocab:
            output.append(model.src_vocab[w])
        else:
            output.append(model.src_vocab['<unk>'])
    output.append(model.src_vocab['</s>'])
    return np.asarray(output, dtype=np.int32)

def num2word(n):
    if n in trg_vocab_reverse:
        return trg_vocab_reverse[n]
    else:
        return '<unk>'

def decode_one(x_batch):
    output = []
    model.encode(x_batch)
    t = chainer.Variable(np.zeros(x_batch.shape[0], dtype=np.int32), volatile='auto')
    
    w = num2word(0)
    while w != '</s>':
        t = model.trg_emb(t)
        o = model.decode(t)
        o = o.data.argmax(axis=1)
        w = num2word(o[0])
        t = np.asarray(o, dtype=np.int32)
        output.append(w)
        if len(output) == opts.genlimit:
            break
        t = chainer.Variable(t, volatile='auto')
    return ' '.join(output)

def decode(text):
    print('--> input:', text, file=sys.stderr)
    text = text2num(text)
    print(decode_one(np.asarray([text], dtype=np.int32)))

if __name__ == "__main__":
    for line in fd_in:
        line = line.strip()
        decode(line)
