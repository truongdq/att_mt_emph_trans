#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 truong-d <truong-d@ahclab40>
#
# Distributed under terms of the MIT license.

"""

"""
from __future__ import print_function
from collections import defaultdict
import operator
import numpy as np
import chainer
from chainer import cuda
import math
import random
import time
import chainer.links as L
import sys
import chainer.functions as F
from donet import donet
from donet.lib_data import load_train_data, load_dev_data, data_spliter, equalize_width, count_word
import optparse

parser = optparse.OptionParser()
parser.add_option('-g', '--gpu', help='GPU ID', default=-1, type=int,
                  dest='gpu')
parser.add_option("--train", dest="fname_train", default=None,
        help="Training data")
parser.add_option("--dev", dest="fname_dev", default=None,
        help="Development data")
parser.add_option("--test", dest="fname_test", default=None,
        help="Testing data")
parser.add_option("--batchsize", dest="bsize", default=1,
        type="int")
parser.add_option("--epoch", dest="epoch", default=1,
        type="int")
parser.add_option("--emb-size", dest="emb_size", default=100,
        type="int")
parser.add_option("--hidden-size", dest="hid_size", default=100,
        type="int")
parser.add_option("--lr", default=0.001, help='Initial learning rate',
        type="float")
parser.add_option("--lr-descend", default=1.8, help='Learning rate descending',
        type="float")
parser.add_option("--lr-stop", default=0.00001, help='Learning rate stop',
        type="float")
parser.add_option("--depth", dest="depth", default=1,
        type="int", help="Number of LSTM layers in both encoder and decoder")
parser.add_option("--drop", dest="drop", default=0,
        type="float", help="Drop ratio")
parser.add_option("--grad-clip", dest="grad_clip", default=5,
        type="int")
parser.add_option("--att-type", default="dot", help="Attention type (dot|general|concat)")
parser.add_option('--bprob-len', type=int, default=35, help='length of truncated BPTT')
parser.add_option('--out-model', help='output model directory')
parser.add_option('--seed', type=int, default=0, help='Random seed')


opts, args = parser.parse_args()
assert opts.fname_train is not None
assert opts.fname_dev is not None

np.random.seed(opts.seed)
random.seed(opts.seed)

xp = cuda.cupy if opts.gpu >= 0 else np

src_vocab = {"<unk>": 0, "</s>": 1, "<stuff>": 2}
trg_vocab = {"<unk>": 0, "</s>": 1, "<stuff>": 2}

train_dataset = load_train_data(opts.fname_train, src_vocab, trg_vocab, unk_thres=1)
dev_dataset = load_dev_data(opts.fname_dev, src_vocab, trg_vocab)
test_dataset = load_dev_data(opts.fname_test, src_vocab, trg_vocab)
print("SRC vocab:", len(src_vocab))
print("TRG vocab:", len(trg_vocab))
w_counts = count_word(train_dataset['target'])
# HSM = L.BinaryHierarchicalSoftmax
# tree = HSM.create_huffman_tree(w_counts)

train_dataset = list(data_spliter(train_dataset, opts.bsize))
dev_dataset = list(data_spliter(dev_dataset, opts.bsize))
test_dataset = list(data_spliter(test_dataset, opts.bsize))


# Define model
emb_size = opts.emb_size
hid_size = opts.hid_size
osize = len(trg_vocab)
# osize = hid_size * 2

nnet = donet.DoNet(emb_size, hid_size, osize, opts.depth, opts.drop, src_vocab, trg_vocab, att_type=opts.att_type)
trainer = donet.Trainer(grad_clip=opts.grad_clip, opt_type="RMSpropGraves", lr=opts.lr, lr_descend=opts.lr_descend, lr_thres=opts.lr_stop)
trainer.setup(nnet)

# loss_func = HSM(osize, tree)

if opts.gpu >=0:
    cuda.get_device(opts.gpu).use()
    nnet.to_gpu()
    # loss_func.to_gpu()

def run_nnet(model, database, train=True, collect_result=False):
    total_loss = 0
    model.train = train
    output = []
    output_mask = []
    for iter_idx, (x_batch, y_batch) in enumerate(database):
        model.reset()
        equalize_width(x_batch, src_vocab)

        y_mask = equalize_width(y_batch, trg_vocab)
        y_mask = np.asarray(y_mask, dtype=np.float32)

        x_batch = np.asarray(x_batch, dtype=np.int32)
        y_batch = np.asarray(y_batch, dtype=np.int32)

        model.encode(x_batch)
    
        t = chainer.Variable(xp.zeros(y_batch.shape[0], dtype=np.int32), volatile='auto')
        
        sen_loss = 0
        sen_output = np.zeros(y_batch.shape, dtype=np.int32)
        for i in xrange(y_batch.shape[1]):
            t = model.trg_emb(t)
            o = model.decode(t)
            t = chainer.Variable(xp.asarray(y_batch[: , i], dtype=np.int32), volatile='auto')
            loss_i = F.softmax_cross_entropy(o, t)
            # loss_i = loss_func(o, t)
            sen_loss += loss_i.data
            if train:
                trainer.add_loss(loss_i)

            if collect_result:
                sen_output[:,i] = cuda.to_cpu(o.data).argmax(axis=1)

            if train and (i + 1) % opts.bprob_len == 0:
                trainer.update_params()

        if collect_result:
            output.append(sen_output)
            output_mask.append(y_mask)
    
        total_loss += (sen_loss) / y_batch.shape[1]

        if train:
            trainer.update_params()
        
        if train:
            iter_idx += 1
            if iter_idx % 10 == 0:
                print("Iter %d / %d lr %.5f loss %.5f" % (iter_idx, len(database), trainer.lr, total_loss / iter_idx), end="\r")
                sys.stdout.flush()
    total_loss /= len(database)

    if not collect_result:
        return total_loss
    else:
        return total_loss, output, output_mask


def parse_output(output, output_mask):
    trg_vocab_reverse = {}
    for w, key in trg_vocab.items():
        trg_vocab_reverse[key] = w

    for sen_b, sen_mask in zip(output, output_mask):
        for sen_id in range(sen_b.shape[0]):
            mask = sen_mask[sen_id]
            sen = sen_b[sen_id]
            
            text = []
            for x, m in zip(sen, mask):
                if m > 0:
                    text.append(trg_vocab_reverse[x]) 
            print(' '.join(text))


if __name__ == "__main__":
    print("Start training at", time.strftime("%c"))
    prev_loss = 10000
    for e in xrange(opts.epoch):
        random.shuffle(train_dataset)
        train_loss = run_nnet(nnet, train_dataset, train=True)
        dev_loss = run_nnet(nnet, dev_dataset, train=False)

        print("%s: Epoch %d lr %.5f train_loss %.5f PPL: %.3f dev_loss %.5f PPL: %.3f" % (time.strftime("%c"), e, trainer.lr, train_loss, math.exp(train_loss), dev_loss, math.exp(dev_loss)))

        if dev_loss > prev_loss:
            trainer.descend_lr()
        prev_loss = dev_loss

        if not trainer.continue_train():
            break
        if opts.out_model:
            nnet.save(opts.out_model)
    test_loss, output, output_mask = run_nnet(nnet, test_dataset, train=False, collect_result=True)
    print("Test_loss %.5f PPL: %.3f" % (test_loss, math.exp(test_loss)))
    print("Training done at", time.strftime("%c"))
    if opts.out_model:
        nnet.save(opts.out_model)
