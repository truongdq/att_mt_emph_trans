# Introduction #
This is the repo of multi-task neural nets.

# Optimizer Adam
SRC vocab: 16844
TRG vocab: 24049
Start training at Sat Jun 11 21:46:36 2016
Sat Jun 11 22:23:21 2016: Epoch 0: train_loss 1.29613 PPL: 3.655 dev_loss 0.93721 PPL: 2.553
Saving model to: exp/donet/donet.mdl
Sat Jun 11 22:58:25 2016: Epoch 1: train_loss 0.87549 PPL: 2.400 dev_loss 0.83619 PPL: 2.308
Saving model to: exp/donet/donet.mdl
Sat Jun 11 23:33:30 2016: Epoch 2: train_loss 0.74577 PPL: 2.108 dev_loss 0.80311 PPL: 2.232
Saving model to: exp/donet/donet.mdl
Sun Jun 12 00:08:34 2016: Epoch 3: train_loss 0.66283 PPL: 1.940 dev_loss 0.80251 PPL: 2.231
Saving model to: exp/donet/donet.mdl
Sun Jun 12 00:43:41 2016: Epoch 4: train_loss 0.60247 PPL: 1.827 dev_loss 0.81510 PPL: 2.259
Saving model to: exp/donet/donet.mdl
Sun Jun 12 01:18:47 2016: Epoch 5: train_loss 0.55534 PPL: 1.743 dev_loss 0.82012 PPL: 2.271
Saving model to: exp/donet/donet.mdl
Sun Jun 12 01:53:54 2016: Epoch 6: train_loss 0.51914 PPL: 1.681 dev_loss 0.83276 PPL: 2.300
Saving model to: exp/donet/donet.mdl
Sun Jun 12 02:29:00 2016: Epoch 7: train_loss 0.48860 PPL: 1.630 dev_loss 0.84379 PPL: 2.325
Saving model to: exp/donet/donet.mdl
Sun Jun 12 03:04:07 2016: Epoch 8: train_loss 0.46443 PPL: 1.591 dev_loss 0.84898 PPL: 2.337
Saving model to: exp/donet/donet.mdl
Sun Jun 12 03:39:15 2016: Epoch 9: train_loss 0.44242 PPL: 1.556 dev_loss 0.88735 PPL: 2.429
Saving model to: exp/donet/donet.mdl
Sun Jun 12 04:14:23 2016: Epoch 10: train_loss 0.42422 PPL: 1.528 dev_loss 0.88734 PPL: 2.429
Saving model to: exp/donet/donet.mdl
Sun Jun 12 04:49:31 2016: Epoch 11: train_loss 0.40783 PPL: 1.504 dev_loss 0.90028 PPL: 2.460
Saving model to: exp/donet/donet.mdl
Sun Jun 12 05:24:37 2016: Epoch 12: train_loss 0.39376 PPL: 1.483 dev_loss 0.90099 PPL: 2.462
Saving model to: exp/donet/donet.mdl
Sun Jun 12 05:59:45 2016: Epoch 13: train_loss 0.38167 PPL: 1.465 dev_loss 0.99285 PPL: 2.699
Saving model to: exp/donet/donet.mdl
Sun Jun 12 06:34:53 2016: Epoch 14: train_loss 0.37002 PPL: 1.448 dev_loss 0.94858 PPL: 2.582
Saving model to: exp/donet/donet.mdl
Test_loss 0.93900 PPL: 2.557
Training done at Sun Jun 12 06:35:19 2016
Saving model to: exp/donet/donet.mdl
